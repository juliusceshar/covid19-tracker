package dev.antoniodvr.covid19tracker.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data",
        "stato",
        "ricoverati_con_sintomi",
        "terapia_intensiva",
        "totale_ospedalizzati",
        "isolamento_domiciliare",
        "totale_attualmente_positivi",
        "nuovi_attualmente_positivi",
        "dimessi_guariti",
        "deceduti",
        "totale_casi",
        "tamponi"
})
public class CountryData implements Auditable {

    @JsonProperty("data")
    private String date;
    @JsonProperty("stato")
    private String country;
    @JsonProperty("ricoverati_con_sintomi")
    private Integer hospitalisedWithSymptoms;
    @JsonProperty("terapia_intensiva")
    private Integer intensiveCare;
    @JsonProperty("totale_ospedalizzati")
    private Integer totalHospitalised;
    @JsonProperty("isolamento_domiciliare")
    private Integer homeConfinement;
    @JsonProperty("totale_positivi")
    private Integer currentPositive;
    @JsonProperty("nuovi_positivi")
    private Integer currentNewPositive;
    @JsonProperty("dimessi_guariti")
    private Integer recovered;
    @JsonProperty("deceduti")
    private Integer death;
    @JsonProperty("totale_casi")
    private Integer totalCases;
    @JsonProperty("tamponi")
    private Integer testPerformed;

    @Override
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getHospitalisedWithSymptoms() {
        return hospitalisedWithSymptoms;
    }

    public void setHospitalisedWithSymptoms(Integer hospitalisedWithSymptoms) {
        this.hospitalisedWithSymptoms = hospitalisedWithSymptoms;
    }

    public Integer getIntensiveCare() {
        return intensiveCare;
    }

    public void setIntensiveCare(Integer intensiveCare) {
        this.intensiveCare = intensiveCare;
    }

    public Integer getTotalHospitalised() {
        return totalHospitalised;
    }

    public void setTotalHospitalised(Integer totalHospitalised) {
        this.totalHospitalised = totalHospitalised;
    }

    public Integer getHomeConfinement() {
        return homeConfinement;
    }

    public void setHomeConfinement(Integer homeConfinement) {
        this.homeConfinement = homeConfinement;
    }

    public Integer getCurrentPositive() {
        return currentPositive;
    }

    public void setCurrentPositive(Integer currentPositive) {
        this.currentPositive = currentPositive;
    }

    public Integer getCurrentNewPositive() {
        return currentNewPositive;
    }

    public void setCurrentNewPositive(Integer currentNewPositive) {
        this.currentNewPositive = currentNewPositive;
    }

    public Integer getRecovered() {
        return recovered;
    }

    public void setRecovered(Integer recovered) {
        this.recovered = recovered;
    }

    public Integer getDeath() {
        return death;
    }

    public void setDeath(Integer death) {
        this.death = death;
    }

    public Integer getTotalCases() {
        return totalCases;
    }

    public void setTotalCases(Integer totalCases) {
        this.totalCases = totalCases;
    }

    public Integer getTestPerformed() {
        return testPerformed;
    }

    public void setTestPerformed(Integer testPerformed) {
        this.testPerformed = testPerformed;
    }
}