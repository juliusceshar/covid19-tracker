'use strict';

(function($) {
    $(document).ready(function() {

        //
        // New positive by region
        //
        var newPositiveByRegion = document.getElementsByClassName('new-positive-by-region')[0];
        new Chart(newPositiveByRegion, {
            type: 'bar',
            data: {
                labels: newPositiveByRegionLabels,
                datasets: [
                    {
                        label: currentNewPositiveLabel,
                        data: newPositiveByRegionSeries,
                        backgroundColor: 'rgba(196,24,44,0.9)',
                    },
                    {
                        label: recoveredLabel,
                        data: recoveredByRegionSeries,
                        backgroundColor: 'rgba(23,198,113,0.9)',
                    },
                    {
                        label: deathLabel,
                        data: deathByRegionSeries,
                        backgroundColor: 'rgba(0,0,0,0.7)',
                    }
                ]
            },
            options: {
                responsive: true,
                legend: {
                    display: false,
                    position: 'top'
                },
                scales: {
                    xAxes: [{
                        gridLines: false,
                        ticks: {
                            autoSkip: false,
                            maxRotation: 90,
                            minRotation: 90
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            suggestedMax: 10,
                            callback: function(tick, index, ticks) {
                                if (tick === 0) {
                                    return tick;
                                }
                                // Format the amounts using Ks for thousands.
                                return tick > 999 ? (tick / 1000).toFixed(1) + 'K' : tick;
                            }
                        }
                    }]
                }
            }
        });
    });
})(jQuery);